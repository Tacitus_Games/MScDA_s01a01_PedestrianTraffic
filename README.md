# Integrated Continuous Assessment 1 for Semester 1 of MSc in Data Analytics.  
**Domain:** Transport.

## Description
**Scenario:** Transport and Infrastructure  
A large amount of data has been collected by Dublin City Council (DCC)
regarding Transport and Infrastructure in the Greater Dublin Area, This data is
available at:  
<https://data.gov.ie/organization/dublin-city-council?tags=Transport+and+Infrastructure>  
You are required to choose a particular area of interest and formulate the
appropriate questions for modelling and analysis.
For Example (but not limited to):

 * Clamping Appeals
 * Multistorey Car Parking Space Availability
 * Telecoms Underground Infrastructure DCC
 * etc…

You are required to collect, process, analyse and interpret the data in order
to identify possible issues/problems at present and make predictions/
classifications in regards to the future.
This analysis will rely on the available data from DCC and any additional data
you deem necessary (with supporting evidence) to support your hypothesis for
this scenario.
This will require you to employ critical analysis of not only the domain of
choice but also for the regression and or classification that you undertake.

## Authors and acknowledgment
Tacitus

## License
Copyright © 2022 Tacitus, all rights reserved
